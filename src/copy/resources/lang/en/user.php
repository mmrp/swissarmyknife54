<?php
return [
    'management' => 'User Management',

    'user' => 'User',
    'users' => 'Users',

    'id' => '#',
    'name' => 'Name',
    'email' => 'E-Mail',
    'password' => 'Password',
    'profile_image' => 'Profile Image',
    'roles' => 'Roles'
];